package com.moro.demo.api;

import com.moro.demo.UserNotFoundException;
import com.moro.demo.entity.User;
import com.moro.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.google.common.collect.Lists;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders="*", methods=RequestMethod.GET)
@RestController
public class UserApi {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/user/delete")
    public void delete(@RequestHeader(value = "id") Integer id) {
        userRepository.deleteById(id);
    }

    @PostMapping("/createuser/add")
    public User add(@RequestBody User user) {
        userRepository.save(user);
        return user;

    }    @PostMapping("/createuser/update")
    public User update(@RequestBody User user) {
        userRepository.save(user);
        return user;
    }

    @GetMapping("/user/getall")
    public ArrayList<User> getAll() {
        return Lists.newArrayList(userRepository.findAll());
    }

    @GetMapping("/user/getbyid")
    public User getById(@RequestHeader(value = "id") Integer id) throws HttpClientErrorException.NotFound {
        Optional<User> user = userRepository.findUserById(id);
        if(user.isPresent()) {
            return user.get();
        } else {
            throw new UserNotFoundException();
        }
    }

    @GetMapping("/user/login")
    public void login() {
    }
}
