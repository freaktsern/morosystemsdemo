package com.moro.demo.service;

import com.moro.demo.entity.User;
import com.moro.demo.model.UserDetailsImpl;
import com.moro.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    //
    public UserDetails loadUserByUsername(String userName)
            throws UsernameNotFoundException {
        Optional<User> user = userRepository.findUserByUsername(userName);


        return user.map(UserDetailsImpl::new).get();
/*
        if (user == null) {
            throw new UsernameNotFoundException(
                    "No user found with username: "+ userName);
        }
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        return  new org.springframework.security.core.userdetails.User
                (user.getUsername(),
                        user.getPassword().toLowerCase(), enabled, accountNonExpired,
                        credentialsNonExpired, accountNonLocked,
                        user.getAuthorities());*/
    }
}
